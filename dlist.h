#ifndef DLIST_H
#define DLIST_H
using namespace std;

class dlist{
public:
    struct node{
        node *next;
        node *prev;
        int element;
        int index;
    } *head;
	dlist(int value);
	~dlist();
	dlist (dlist &p);
	void addBeginning(int value);
	void addAfterE(int after, int value);
	void addAtI(int At, int value);
	void deleteE(int value);
	void deleteI(int index);
	void deleteFromTo(int lower, int higher);
	void read();
	void readI(int index);
	void readE(int element);
	void destroy();
	void clean();
	int getSize();
	void operator+(dlist d);
	void operator-(dlist d);
	bool operator<(dlist d);
};
#endif
