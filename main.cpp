#include <iostream>
#include "tree.h"
#include "dlist.h"
using namespace std;

//Testy do klasy tree
int main(){
cout << "Liczby calkowite" << endl;
cout << "Dodawanie: 1, 2, 3, 4, 5, 6, 7, 10, 15, 20, 25, 30, 35, 40" << endl;
tree<int> A;
if(A.add(1) && A.add(2) && A.add(3) && A.add(4) && A.add(5) && A.add(6) && A.add(7) && A.add(10) && A.add(15) && A.add(20) && A.add(25) && A.add(30) && A.add(35) && A.add(40)) cout << "Dodawanie funkcjonalne" << endl;
    else cout << "Blad dodawania" << endl;
if(A.checkVal(10)) cout << "Sprawdzanie przynaleznosci funkcjonalne" << endl;
else cout << "Blad sprawdzania przynaleznosci" << endl;
if(A.nodeCount()== 11 && A.treeLevel() == 3) cout << "Sprawdzanie wielkosci funkcjonalne" << endl;
else cout << "Blad sprawdzania wielkosci" << endl;
A.printPre();
treeIterator<int> a(A);
int a1 = a.current->value;
++a;
++a;
++a;
int a2 = a.current->value;
if(a1 == 1 && a2 == 4) cout << "Iterator funkcjonalny" << endl;
    else cout << "Blad iteratora" << endl;
a.printTree();

cout << endl << endl <<"Lancuch znakow" << endl;

cout << "Dodawanie: a, b, c, d, e, ef, aa, aab, abf, ggw, dz, ds, aabb, xyz" << endl;
tree<string> B;
if(B.add("a") && B.add("b") && B.add("c") && B.add("d") && B.add("e") && B.add("ef") && B.add("aa") && B.add("aab") && B.add("abf") && B.add("ggw") && B.add("dz") && B.add("ds") && B.add("aabb") && B.add("xyz")) cout << "Dodawanie funkcjonalne" << endl;
    else cout << "Blad dodawania" << endl;
if(B.checkVal("aab")) cout << "Sprawdzanie przynaleznosci funkcjonalne" << endl;
else cout << "Blad sprawdzania przynaleznosci" << endl;
if(B.nodeCount()== 8 && B.treeLevel() == 3) cout << "Sprawdzanie wielkosci funkcjonalne" << endl;
else cout << "Blad sprawdzania wielkosci" << endl;
B.printPre();
treeIterator<string> b(B);
string b1 = b.current->value;
++b;
++b;
++b;
string b2 = b.current->value;
if(b1 == "a" && b2 == "aabb") cout << "Iterator funkcjonalny" << endl;
    else cout << "Blad iteratora" << endl;
b.printTree();
return 0;
};
