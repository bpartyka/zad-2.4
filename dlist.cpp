#include <iostream>
#include "dlist.h"
using namespace std;

dlist::dlist(int value){//konstruktor
        head = new node;
        head->index = 1;
        head->element = value;
        head->prev = NULL;
        head->next = NULL;
    };

dlist::~dlist(){};
//destruktor
dlist::dlist (dlist &p){//konstruktor kopiujący
        while(p.head->prev != NULL)
            p.head = p.head->prev;
        head = new node;
        head->index = 1;
        head->element = p.head->element;
        head->prev = NULL;
        head->next = NULL;
        while(1){//elementy po headerze
            if(p.head->next != NULL)
            {
                p.head = p.head->next;
                addBeginning(p.head->element);
            }
            else{
                break;
            }
        }
        };

void dlist::addBeginning(int value){//dodawanie elementu value na koniec listy
        node *n = new node;
        n->element = value;
        n->index = head->index + 1;
        n->next = NULL;
        n->prev = head;
        head->next = n;
        head = n;
	};

void dlist::addAfterE(int after, int value){// dodawanie elementu value po elemencie after
        node *n = new node;
        while(head->element != after)
        {
            if(head->prev == NULL)
            {
                cout << "No such element" << endl;
                delete n;
                while(head->next != NULL)
                {
                    head = head->next;
                }
                return;
            }
            head=head->prev;
        }
        n->next = NULL;
        n->element = value;
        n->index = head->index + 1;
        if(head->next != NULL){
        n->next = head->next;
        n->next->prev = n;}
        head->next = n;
        n->prev = head;
        head = n;
        while(head->next != NULL)
        {
            head = head->next;
        if(head->index == head->prev->index)
        head->index = head->index + 1;
        else
        {
            head = head->next;

        }
        }
        };

void dlist::addAtI(int At, int value){//dodawanie elementu pod indeks o numerze At
        if(At < 0)
            At = -At;
        node *n = new node;
        while(head->index >= At)
        {
            if(head->prev == NULL)
            {
                n->next = head;
                n->prev = NULL;
                n->element = value;
                n->index = At;
                head->prev = n;
                while(head->next != NULL)
                {
                    head = head->next;
                }
                return;
            }
            head=head->prev;
        }
        n->next = NULL;
        n->element = value;
        n->index = At;
        if(head->next != NULL){
        n->next = head->next;
        n->next->prev = n;}
        head->next = n;
        n->prev = head;
        head = n;
        while(head->next != NULL)
        {
            head = head->next;
        if(head->index == head->prev->index)
        head->index = head->index + 1;
        else
        {
            head = head->prev;
            break;
        }
        }
        };

void dlist::deleteE(int value){//usuwa wszystkie elementy value z danej listy
        node *temp;
        int t = 0;
        while(1)
        {
            if(head->element == value)
            {
                if(head->prev != NULL)
                {
                    head->prev->next = head->next;
                }
                if(head->next != NULL)
                {
                    head->next->prev = head->prev;
                }
                temp = head;
                if(head->next != NULL)
                    head = head->next;
                else
                {
                    head = head->prev;
                    t = 1;

                }
                delete temp;
            }
            if(head->prev == NULL)
            {
                while(head->next != NULL)
                {
                    head = head->next;
                }
                return;
            }
            if(t == 0)
                head=head->prev;
            t = 0;
        }

        };

void dlist::deleteI(int index){//usuwa element o indeksie index
        node *temp;
        int t;
        while(1)
        {
            if(head->index == index)
            {
                if(head->prev != NULL)
                {
                    head->prev->next = head->next;
                }
                if(head->next != NULL)
                {
                    head->next->prev = head->prev;
                }
                temp = head;
                if(head->next != NULL)
                    head = head->next;
                else
                {
                    head = head->prev;
                    t = 1;

                }
                delete temp;
            }
            if(head->prev == NULL)
            {
                while(head->next != NULL)
                {
                    head = head->next;
                }
                return;
            }
            if(t == 0)
                head=head->prev;
            t = 0;
        }
        };

void dlist::deleteFromTo(int lower, int higher){//usuwa wszystkie elementy w zakresie od lower do higher
        if(higher<lower)
        {
            cout << "Error" << endl;
            return;
        }
        node *temp;
        int t;
        while(1)
        {
            if(head->element >= lower && head->element <= higher){
                if(head->prev != NULL)
                {
                    head->prev->next = head->next;
                }
                if(head->next != NULL)
                {
                    head->next->prev = head->prev;
                }
                temp = head;
                if(head->next != NULL)
                    head = head->next;
                else
                {
                    head = head->prev;
                    t = 1;

                }
                delete temp;
            }
            if(head->prev == NULL)
            {
                while(head->next != NULL)
                {
                    head = head->next;
                }
                return;
            }
            if(t == 0)
                head=head->prev;
            t = 0;
        }
    };

void dlist::read(){//podglad listy
        while(head->prev != NULL)
            head=head->prev;
        while(1){
        cout << "index:" << head->index << " value:" << head->element << endl;
        if(head->next != NULL)
            head = head->next;
        else
            break;
        }
        cout << "..........................." << endl;
        };

void dlist::readI(int index){//podglad indeksu numer index
        while(head->index != index)
            head=head->prev;
        cout << "index:" << head->index << " value:" << head->element << endl;
        while (head->next != NULL)
            head = head->next;
        cout << "..........................." << endl;
        };

void dlist::readE(int element){//podglad elementu element
        while(head->element != element)
            head=head->prev;
        cout << "index:" << head->index << "value:" << head->element << endl;
        while (head->next != NULL)
            head = head->next;
        cout << "..........................." << endl;
        };

void dlist::destroy(){//usuwa liste
        node *temp;
        while(head->prev != NULL)
            head = head->prev;
        while(1){
            if(head->next != NULL){
                temp = head;
                head = head->next;
                delete temp;
            }
            else
            {
                delete head;
                break;
            }
        }
        delete this;
    };

void dlist::clean(){//funkcja usuwa powtarzajace sie wartosci w liscie
        node * temp1, * temp2;
        while(head->prev != NULL)
            head=head->prev;
        while(1){
            temp1 = head->next;
            while(1){
                if(temp1 == NULL)
                    break;

                    if(temp1->element == head->element)
                      {
                        if(temp1->prev != NULL)
                        {
                            temp1->prev->next = temp1->next;
                        }
                        if(temp1->next != NULL)
                        {
                            temp1->next->prev = temp1->prev;
                        }
                        temp2 = temp1;
                        if(temp1->next != NULL){
                            temp1 = temp1->next;
                            temp1->index = temp1->index - 1;}
                        else
                        {
                            temp1 = temp1->prev;
                        }
                        delete temp2;
                      }
                    temp1 = temp1->next;


            }
        if(head->next != NULL)
            head = head->next;
        else
            break;
        }
        cout << "List cleaned" << endl;
        };

int dlist::getSize(){//zwraca ilosc elementow w liscie
        int i = 1;
        while(head->prev != NULL)
            head=head->prev;
        while(head->next != NULL){
            head = head->next;
            i++;
        }
        return i;
        };

void dlist::operator+(dlist d){//operator laczenia list
        while(d.head->prev != NULL)
            d.head = d.head->prev;
        while(1){//elementy z listy d sa dodane po kolei na poczatek obecnej listy
            addBeginning(d.head->element);//uzyta funkcja prostego dodawania
            if(d.head->next != NULL)
                d.head = d.head->next;
            else
                break;
        }
        };

void dlist::operator-(dlist d){//operator odejmowania list
        while(d.head->prev != NULL)
            d.head = d.head->prev;
        while(1){//wszystkie elementy wystepujace w liscie d sa usuniete z obecnej listy
            deleteE(d.head->element);//uzyta funkcja odejmowania elementu
            if(d.head->next != NULL)
                d.head = d.head->next;
            else
                break;
        }

        while(head->prev != NULL){
            head = head->prev;
        }
        while(1){
            if(head->next != NULL){
                head->next->index = head->index + 1;
                head = head->next;
                }
            else
                break;
        }
    };

bool dlist::operator<(dlist d){
    int s1, s2;
    s1 = getSize();
    s2 = d.getSize();
    if(s1 < s2) return true;
    else return false;

    };