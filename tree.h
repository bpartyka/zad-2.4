//Bartłomiej Partyka 2I2, Zadanie 2.4
//1.4.18

#ifndef TREE_H
#define TREE_H
#include <iostream>
using namespace std;

template<class keyType>
class treeIterator;

template<class keyType>
class node{

    public:
    int keyNum;
    keyType first, second, third;
    node<keyType> * left, * middleLeft, * middleRight, * right, * parent;
    node(keyType key){
        keyNum = 1;
        first = key;
        left = NULL;
        middleLeft = NULL;
        middleRight = NULL;
        right = NULL;
        parent = NULL;
        }
    ~node(){
        delete left;
        delete middleLeft;
        delete middleRight;
        delete right;
    }
};//node

    template<class keyType>
    class tree{

    public:
    tree(){root = NULL;};

    ~tree(){delete root;};


    bool add(keyType item);//add
    void printPre();//graficzne przedstawienie drzewa
    node<keyType> * findVal(keyType value);//szukanie value
    bool checkVal(keyType value);//sprawdzenie czy drzewo zawiera value
    void deleteVal(keyType value);//usuawnie value
    int nodeCount();//ilosc wezlow
    int treeLevel();//poziom drzewa

    /*tree<keyType> operator+(const tree<keyType> &a){
        tree<keyType> sum;
        treeIterator<keyType> A(a);
        treeIterator<keyType> B(this);
        while(1){
            sum.add(A.current->value);
            ++A;
            if(A.current->next == NULL){
                sum.add(A.current->value);
                break;
            }
        }														operator+(?)
        while(1){
            sum.add(B.current->value);
            ++B;
            if(B.current->next == NULL){
                sum.add(B.current->value);
                break;
            }
        }
        return sum;
    }*/
    private:
    node<keyType> * root;

    node<keyType> * findLeaf(keyType item, node<keyType> * leaf);
    void insertVal(keyType value, node<keyType> * curr, node<keyType> * leftChild, node<keyType> * rightChild);
    void printPreNode(node<keyType> * cur, int level);
    void insertToParent(node<keyType> * parent, keyType up, node<keyType> * leftChild, node<keyType> * rightChild);//      funkcje pomocnicze
    node<keyType> * findValue(keyType value, node<keyType> * curr);
    int nCount(node<keyType> * curr);

    friend class treeIterator<keyType>;
//    friend tree<keyType> operator+<>(const tree<keyType> &a, const tree<keyType> &b);

    };//tree

    template<class keyType>
    class itNode{
    public:
        keyType value;
        itNode<keyType> * next;
        ~itNode(){delete next;}
    };//node - iterator

    template<class keyType>
    class treeIterator{
    public:
        itNode<keyType> * current, * head = NULL;
        treeIterator(tree<keyType> T){
            createNode(T.root);
            current = head;
        };

        ~treeIterator(){delete head;};
        void printTree();//wyswietlenie drzewa jako listy
        void operator++();//poruszanie sie po drzewie
    private:
        void createNode(node<keyType> * curr);
        void printNode(itNode<keyType> * curr);
    };//iterator








    //functions



    template<class keyType>
    void treeIterator<keyType>::createNode(node<keyType> * curr){
        if(curr->left != NULL) createNode(curr->left);
        if(head == NULL){
            head = new itNode<keyType>;
            current = head;
            current->value = curr->first;
            current->next = NULL;
        }else{
            current->next = new itNode<keyType>;
            current = current->next;
            current->value = curr->first;
            current->next = NULL;
        }
        if(curr->middleLeft != NULL) createNode(curr->middleLeft);
        if(curr->keyNum > 1){
            current->next = new itNode<keyType>;
            current = current->next;
            current->value = curr->second;
            current->next = NULL;
        }else return;
        if(curr->middleRight != NULL) createNode(curr->middleRight);
        if(curr->keyNum >2){
            current->next = new itNode<keyType>;
            current = current->next;
            current->value = curr->third;
            current->next = NULL;
        }else return;
        if(curr->right != NULL) createNode(curr->right);
        return;
    }// to constructor

    template<class keyType>
    void treeIterator<keyType>::printTree(){
     printNode(head);
     return;
    }

    template<class keyType>
    void treeIterator<keyType>::printNode(itNode<keyType> * curr){
        cout << curr->value << endl;
        if(curr->next != NULL) printNode(curr->next);
        return;
    }

    template<class keyType>
    void treeIterator<keyType>::operator++(){
        if(this->current->next != NULL) this->current = this->current->next;
        else this->current = this->head;
        return;
    }


//iterator functions



    template<class keyType>
   bool tree<keyType>::add(keyType item){
    if(root == NULL){
        root = new node<keyType>(item);
         if(root) return true;
         else return false;
        }
    else{
        node<keyType> * leaf = findLeaf(item, root);
        insertVal(item, leaf, NULL, NULL);
        if(leaf) return true;
        else return false;
    }

    };
    template<class keyType>
    node<keyType> * tree<keyType>::findLeaf(keyType item, node<keyType> * cur){
        if(item < cur->first){if(cur->left == NULL) return cur; else return findLeaf(item, cur->left);}
        else if(cur->keyNum == 1 || item < cur->second){if(cur->middleLeft == NULL) return cur; else return findLeaf(item, cur->middleLeft);}
        else if(cur->keyNum == 2 || item < cur->third){if(cur->middleRight == NULL) return cur; else return findLeaf(item, cur->middleRight);}
        else{if(cur->right == NULL)return cur; else return findLeaf(item, cur->right);}

    };
    template<class keyType>
    void tree<keyType>::insertVal(keyType value, node<keyType> * curr, node<keyType> * leftChild, node<keyType> * rightChild){
    if(curr->keyNum == 1){

        if(value < curr->first){
            curr->second = curr->first;
            curr->first = value;
            curr->middleRight = curr->middleLeft;
            curr->left = leftChild;
            curr->middleLeft = rightChild;
        }else{
            curr->second = value;
            curr->middleLeft = leftChild;
            curr->middleRight = rightChild;
        }
        curr->keyNum++;
        return;
    }
    if(curr->keyNum == 2){
        if(value < curr->first){
            curr->third = curr->second;
            curr->second = curr->first;
            curr->first = value;
            curr->right = curr->middleRight;
            curr->middleRight = curr->middleLeft;
            curr->middleLeft = rightChild;
            curr->left = leftChild;
        }
        else if(value < curr->second){
            curr->third = curr->second;
            curr->second = value;
            curr->right = curr->middleRight;
            curr->middleRight = rightChild;
            curr->middleLeft = leftChild;
        }else{
            curr->third = value;
            curr->right = rightChild;
            curr->middleRight = leftChild;
        }
        curr->keyNum++;
        return;
    }
    if(curr->keyNum == 3){
    keyType a, b, c, d;
    node<keyType> * c1,* c2,* c3,* c4,* c5;
    if(value < curr->first){
    a = value;
    b = curr->first;
    c = curr->second;
    d = curr->third;
    c1 = leftChild;
    c2 = rightChild;
    c3 = curr->middleLeft;
    c4 = curr->middleRight;
    c5 = curr->right;
    }
    else if(value < curr->second){
        a = curr->first;
        b = value;
        c = curr->second;
        d = curr->third;
        c1 = curr->left;
        c2 = leftChild;
        c3 = rightChild;
        c4 = curr->middleRight;
        c5 = curr->right;
    }
    else if(value < curr->third){
        a = curr->first;
        b = curr->second;
        c = value;
        d = curr->third;
        c1 = curr->left;
        c2 = curr->middleLeft;
        c3 = leftChild;
        c4 = rightChild;
        c5 = curr->right;
    }
    else{
        a = curr->first;
        b = curr->second;
        c = curr->third;
        d = value;
        c1 = curr->left;
        c2 = curr->middleLeft;
        c3 = curr->middleRight;
        c4 = leftChild;
        c5 = rightChild;
    }

    node<keyType> * target = curr->parent;

    node<keyType> * left = new node<keyType>(a);
    left->parent = target;
    left->left = c1;
    left->middleLeft = c2;
    if(c1 != NULL) c1->parent = left;
    if(c2 != NULL) c2->parent = left;
    node<keyType> * right = new node<keyType>(c);
    right->second = d;
    right->keyNum = 2;
    right->parent = target;
    right->left = c3;
    right->middleLeft = c4;
    right->middleRight = c5;
    if(c3 != NULL) c3->parent = right;
    if(c4 != NULL) c4->parent = right;
    if(c5 != NULL) c5->parent = right;

    delete curr;
    insertToParent(target, b, left, right);

   }


    };

    template<class keyType>
    void tree<keyType>::insertToParent(node<keyType> * parent, keyType up, node<keyType> * leftChild, node<keyType> * rightChild){
    if(parent == NULL){
        root = new node<keyType>(up);
        root->left = leftChild;
        root->middleLeft = rightChild;
        leftChild->parent = root;
        rightChild->parent = root;
    }
    else
        insertVal(up,parent, leftChild, rightChild);
    }

    template<class keyType>
    void tree<keyType>::printPre(){
    printPreNode(root, 0);
    cout << endl << endl;
    return;
    };

    template<class keyType>
    void tree<keyType>::printPreNode(node<keyType> * cur, int level){
    if(cur == NULL) return;

        for(int i = 0;i < level; i++){
        cout << "- ";
    }

    if (cur->keyNum==3) cout << cur->keyNum << " {" << cur->first << " " << cur->second << " " << cur->third << "}" << endl;
    else if(cur->keyNum == 2) cout << cur->keyNum << " {" << cur->first << " " << cur->second << "}" << endl;
    else cout << cur->keyNum << " {" << cur->first << "}" << endl;
    printPreNode(cur->left, level + 1);
    printPreNode(cur->middleLeft, level + 1);
    printPreNode(cur->middleRight, level + 1);
    printPreNode(cur->right, level + 1);
    return;
    };

    template<class keyType>
    node<keyType> * tree<keyType>::findVal(keyType value){
    if(root != NULL){
        return findValue(value, root);}
    else
        return NULL;
    }//relies on helper function

    template<class keyType>
    node<keyType> * tree<keyType>::findValue(keyType value, node<keyType> * curr){
    if(curr == NULL)
        return NULL;

    if(curr->keyNum == 1){
        if(curr->first == value) return curr;
        else if(value < curr->first) return findValue(value, curr->left);
        else return findValue(value, curr->middleLeft);
    }
    if(curr->keyNum == 2){
        if(curr->first == value || curr->second == value) return curr;
        else if(value < curr->first) return findValue(value, curr->left);
        else if(value < curr->second) return findValue(value, curr->middleLeft);
        else return findValue(value ,curr->middleRight);
    }
    if(curr->keyNum == 3){
        if(curr->first == value || curr->second == value || curr->third == value) return curr;
        else if(value < curr->first) return findValue(value, curr->left);
        else if(value < curr->second) return findValue(value, curr->middleLeft);
        else if(value < curr->third) return findValue(value, curr->middleRight);
        else return findValue(value, curr->right);
    }
    };//searching function, 3 cases depending on keyNum

    template<class keyType>
    bool tree<keyType>::checkVal(keyType value){
        if(findVal(value)) return true;
        else return false;
    }




    template<class keyType>
    void tree<keyType>::deleteVal(keyType value){
    node<keyType> * toDelete = findVal(value);
    if(toDelete->left == NULL && toDelete->middleLeft == NULL && toDelete->middleRight == NULL && toDelete->right == NULL){
        if(toDelete->keyNum > 1){
            if(value == toDelete->first){
                toDelete->first = toDelete->second;
                toDelete->second = toDelete->third;
                toDelete->keyNum--;
            }else if(value == toDelete->second){
                toDelete->second = toDelete->third;
                toDelete->keyNum--;
            }else toDelete->keyNum--;
        return;
        }else{
            //unfinished
        }
    }
    };


    template<class keyType>
    int tree<keyType>::nodeCount(){
        int i = nCount(root);
        return i;
    };

    template<class keyType>
    int tree<keyType>::nCount(node<keyType> * curr){
        int i = 0;
        if(curr->left != NULL) i+=nCount(curr->left);
        if(curr->middleLeft != NULL) i+=nCount(curr->middleLeft);
        if(curr->middleRight != NULL) i+=nCount(curr->middleRight);
        if(curr->right != NULL) i+=nCount(curr->right);
        i++;
        return i;

    }


    template<class keyType>
    int tree<keyType>::treeLevel(){
        int i;
        node<keyType> * curr = root;
        for(i = 1;curr->left != NULL;curr = curr->left) i++;//works since all leaves are on the same level
        return i;
    };



/*
    template<class keyType>
    tree<keyType> operator+(const tree<keyType> &a, const tree<keyType> &b){
        tree<keyType> sum;
        treeIterator<keyType> A(a);
        treeIterator<keyType> B(b);
        while(1){
            sum.add(A.current->value);
            ++A;
            if(A.current->next == NULL){
                sum.add(A.current->value);
                break;
            }
        }
        while(1){
            sum.add(B.current->value);
            ++B;
            if(B.current->next == NULL){
                sum.add(B.current->value);
                break;
            }
        }
        return sum;
    }
*/



    //tree functions
#endif // TREE_H